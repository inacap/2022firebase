package dev.david.pruebas.holamundo2b50v.modelos;

import com.google.firebase.firestore.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Usuario {
    public String tipo;
    public String nombre;
    public String email;

    public Usuario() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Usuario(String tipo, String nombre, String email) {
        this.tipo = tipo;
        this.nombre = nombre;
        this.email = email;
    }
}
