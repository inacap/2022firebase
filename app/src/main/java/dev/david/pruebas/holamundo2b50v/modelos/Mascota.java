package dev.david.pruebas.holamundo2b50v.modelos;

import com.google.firebase.firestore.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Mascota {
    public String tipo;
    public String nombre;
    public String dueno;

    public Mascota() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Mascota(String tipo, String nombre, String dueno) {
        this.tipo = tipo;
        this.nombre = nombre;
        this.dueno = dueno;
    }
}
