package dev.david.pruebas.holamundo2b50v;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegistrarActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);

        // Iniciar app Firebase
        FirebaseApp.initializeApp(this);

        // Instanciar modulo de Autenticacion
        mAuth = FirebaseAuth.getInstance();

        Button btCrear = findViewById(R.id.btCrear);
        btCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Validar datos
                EditText etEmail = findViewById(R.id.etEmail);
                String email = etEmail.getText().toString();

                if(email.isEmpty()){
                    Toast.makeText(RegistrarActivity.this, "Debe especificar un nombre de email", Toast.LENGTH_SHORT).show();
                    return;
                }

                EditText etPass = findViewById(R.id.etPassword);
                String pass = etPass.getText().toString();

                if(pass.isEmpty()){
                    Toast.makeText(RegistrarActivity.this, "Debe especificar una contraseña", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(pass.length() < 6){
                    Toast.makeText(RegistrarActivity.this, "Contraseña muy corta", Toast.LENGTH_SHORT).show();
                    return;
                }

                EditText etPassRep = findViewById(R.id.etPasswordRep);
                String passRep = etPassRep.getText().toString();

                if(!pass.equals(passRep)){
                    Toast.makeText(RegistrarActivity.this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
                    return;
                }

                // Superdas todas las validaciones, creamos la cuenta en Firebase
                mAuth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(RegistrarActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Toast.makeText(RegistrarActivity.this, "Cuenta creada", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(RegistrarActivity.this, "Error al crear la cuenta", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }
}