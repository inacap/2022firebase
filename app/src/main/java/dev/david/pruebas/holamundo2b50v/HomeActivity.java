package dev.david.pruebas.holamundo2b50v;

import androidx.appcompat.app.AppCompatActivity;
import dev.david.pruebas.holamundo2b50v.modelos.Mascota;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

public class HomeActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseFirestore mDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        FirebaseApp.initializeApp(this);

        // Modulo de autenticacion
        this.mAuth = FirebaseAuth.getInstance();

        // // Modulo de base de datos
        this.mDB = FirebaseFirestore.getInstance();

        TextView tvUsuario = findViewById(R.id.tvUsuario);
        tvUsuario.setText(this.mAuth.getCurrentUser().getEmail());

        Button btGuardar = findViewById(R.id.btGuardar);
        btGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Se toma el email del usuario que inicio sesion
                String email = mAuth.getCurrentUser().getEmail();

                // Se crea un objeto de la entidad que queremos guardar
                // ver capa modelos
                Mascota mascota = new Mascota("gato", "michi", email);

                // Se alamcena en Firebase en la coleccion mascotas
                mDB.collection("mascotas").add(mascota);
                Toast.makeText(HomeActivity.this, "Mascota guardada", Toast.LENGTH_SHORT).show();
            }
        });

        Button btCerrar = findViewById(R.id.btLogout);
        btCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Esto es para cerrar sesion en Firebase
                mAuth.signOut();
                Intent i = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });

        Button btConsultar = findViewById(R.id.btConsultar);
        btConsultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Pedir una lista de los documentos en la coleccion
                // la funcion whereEqualTo permite hacer un filtrado
                // En este caso donde el atributo dueno sea igual al
                // email del usuario que inicio sesion

                mDB.collection("mascotas").whereEqualTo("dueno", mAuth.getCurrentUser().getEmail()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<DocumentSnapshot> lista = queryDocumentSnapshots.getDocuments();
                        for(int i = 0; i < lista.size(); i++){
                            Mascota m = lista.get(i).toObject(Mascota.class);
                            Log.i("ITEM", "MASCOTA: TIPO: " + m.tipo);
                            Log.i("ITEM", "MASCOTA: DUENO: " + m.dueno);
                        }
                    }
                });
            }
        });
    }
}












